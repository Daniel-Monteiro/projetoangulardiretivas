import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-segundo-componente',
  templateUrl: './segundo-componente.component.html',
  styleUrls: ['./segundo-componente.component.css']
})
export class SegundoComponenteComponent implements OnInit {

  pessoas = [
    {nome:"Mark", sobrenome:"Zuckerberg"  },
    {nome:"Ada", sobrenome:"Lovelace"  },
    {nome:"Donald", sobrenome:"Knuth"  },
    {nome:"Grace", sobrenome:"Murray"  },
    {nome:"Ray", sobrenome:"Tomlinson"  },
    {nome:"Dennis", sobrenome:"Ritchie"  },
    {nome:"Linus", sobrenome:"Torvalds"  }
  ];

  constructor() { }

  ngOnInit() {

    }
  }
}

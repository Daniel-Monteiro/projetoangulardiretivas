import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-primeiro-componente',
  templateUrl: './primeiro-componente.component.html',
  styleUrls: ['./primeiro-componente.component.css']
})
export class PrimeiroComponenteComponent implements OnInit {

  url: string = "https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Angular_full_color_logo.svg/1000px-Angular_full_color_logo.svg.png"

  status: boolean = false;

  constructor() { }

  ngOnInit() {

  }

  salvarRegistro(email, senha, endereco, endereco_sec, cidade, estado){

    if(email == null || email == "" || senha == null || senha == ""){

        alert("Verifique os campos de Email e Senha");
        
    }else{
      this.status = true;
    }
  }
}
